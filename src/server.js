const MAX_PLAYERS = 2;
const NUM_BOTS = 0;
//
let http = require('http');
let nstatic = require('node-static');
let ws = require('ws');
require('console-stamp')(console, {pattern: 'yyyy-mm-dd HH:MM:ss.L'});
//
// Http
//
let fileServer = new nstatic.Server('./public', {
    cache: false,
    gzip: true
});

const kPort = 8080;
http.createServer((req, res) => {
    req.on('error', (err) => {
        console.error(err);
    });
    req.on('data', (data) => {

    });
    req.on('end', () => {
        fileServer.serve(req, res);
    });
}).listen(kPort, () => {
    console.log('Static fileserver running on port', kPort);
});

//MY VECTOR SHIT
class Vector2 {

    constructor(x, y) {
        this.x = x || 0;
        this.y = y || 0;
    }

    dot(v) {
        return this.x * v.x + this.y * v.y;
    }

    lerp( t, v )
    {
        if (v instanceof Vector2) {
            this.x = lerp( t, this.x, v.x );
            this.y = lerp( t, this.y, v.y );
        }
        else
            throw new Error('lerp argument not vector2');
    }

    add(v) {
        if (v instanceof Vector2) {
            this.x += v.x; 
            this.y += v.y;
        }
        else
            throw new Error('add argument is not vector2');
    }

    length() {
        return Math.sqrt(this.dot(this));
    }

    distanceTo( a )
    {
        return Math.sqrt( (this.x - a.x) * (this.x - a.x) + (this.y - a.y) * (this.y - a.y) );
    }

    distanceToSqr( a )
    {
        return (this.x - a.x) * (this.x - a.x) + (this.y - a.y) * (this.y - a.y);
    }

    clone() {
        return new Vector2( this.x, this.y );
    }

    set (v){
        if (v instanceof Vector2) {
            this.x = v.x; 
            this.y = v.y;
        }
        else
            throw new Error('set argument not vector2');
    }

    subtract(v) {
        if (v instanceof Vector2) {
            this.x -= v.x; 
            this.y -= v.y;
        }
        else
            throw new Error('subtract argument not vector2');
    }

    normalize() {
        let l = this.length();
        if( l > 0 )
            this.divideScalar( this.length() );
    }

    divideScalar(v) {
        if( v !== 0 )
        {
            this.x /= v;
            this.y /= v;
        }
        else
            throw new Error('divide scalar by 0');
       
    }

    multiplyScalar(v) {
        this.x *= v;
        this.y *= v;
    }

    wrap( size ) {
        let r = false;
        //x
        if( this.x > size * 0.5 ){
            this.x -= size;
            r = true;
        } 
        else if( this.x < size * -0.5 ){ 
            this.x += size;
            r = true;
        }
        //y
        if( this.y > size * 0.5 ){
            this.y -= size;
            r = true;
        } 
        else if( this.y < size * -0.5 ){ 
            this.y += size;
            r = true;
        }

        this.x = Math.max( size * -0.5, Math.min( this.x, size * 0.5 ) );
        this.y = Math.max( size * -0.5, Math.min( this.y, size * 0.5 ) );

        //
        return r;
    }
}

let bytesIn = 0;
let bytesOut = 0;

let names = ["Asher","Atticus","Auden","August","Axl","Beckett","Brax","Bruno","Byron","Dashiell","Dexter","Django","Duke","Edison","Elvis","Everitt","Fenton","Fitz","Frances","Greer","Gulliver","Gus","Holden","Homer","Hugo","Ignatius","Ike","Jagger","Jasper","Kingston","Leopold","Levi","Lionel","Luca","Magnus","Miles","Milo","Monty","Moses","Nico","Nix","Odin","Orlando","Orson","Oscar","Otis","Otto","Poppy","Prince","Ray","Roman","Roscoe","Rufus","Salinger","Sebastian","Stella","Sullivan","Tennyson","Theo","Waldo","Zane","Zeus"];

class Player {

    constructor() {
        this.socket = null;
        this.id = 0;
        this.boss = null;
        this.active = false;
        this.bot = false;
        let first = names[ Math.floor( Math.random() * names.length ) ];
        let last = names[ Math.floor( Math.random() * names.length ) ];
        this.name = first+" "+last;
        this.ready = false;
    }

    serialize(dataView, offset) {
        //id
        dataView.setUint8(offset, this.id );
        offset += 1;
        dataView.setUint8(offset, ( ( this.active && this.ready ) ? 1 : 0 ) );
        offset +=1;
        let nameLength = this.name.length;
        if( nameLength > 15 ) nameLength = 15; //max 
        dataView.setUint8(offset, nameLength );
        offset += 1;
        for (let x = 0; x < nameLength; x++)
        {
            dataView.setUint8(offset, this.name.charCodeAt(x) );
            offset += 1;
        }
        //16 bytes max for name
        //2 bytes for everything else
        //18 total
        return offset;
    }

    setSocket(socket) {
        this.socket = socket;

        socket.binaryType = "arraybuffer";
        socket.on('error', (error) => {
            console.log(error);
            this.boss.free(this);
        });
        socket.on('close', (code, message) => {
            console.log("closed: (%d): %s", code, message);
            this.boss.free(this);
        });
        socket.on('message', (data, flags) => {
            // Process the player's message
            {
                // For similarity with the client, we want data in ArrayBuffer form 
              
                let arrayBuffer = null;
                if (typeof(data) === "object") {
                    if (data instanceof ArrayBuffer) { arrayBuffer = data; }
                    if (data instanceof Buffer) {
                        arrayBuffer = new ArrayBuffer(data.length);
                        let view = new Uint8Array(arrayBuffer);
                        for (let i = 0; i < data.length; i++) {
                            view[i] = data[i];
                        }
                    }
                }

                if (arrayBuffer && arrayBuffer.byteLength > 0) {
                    let dataView = new DataView(arrayBuffer);
                    let offset = 0;
                    //eventually input position for each player...
                   
                    this.ready = dataView.getUint8(offset) === 1;
                    offset += 1;
                    let nameLength = dataView.getUint8(offset);
                    offset += 1;
                    let name = "";
                    for (let x = 0; x < nameLength; x++){
                        name += String.fromCharCode( dataView.getUint8(offset) );
                        offset += 1;
                    }
                    this.name = name;

                    bytesIn += offset;
                }
            }

            // Send them back an updated game state
        
            let playerSize = 18 * MAX_PLAYERS;
            let arrayBuffer = new ArrayBuffer( playerSize + 1 ); // plus 1 for local player id
            let dataView = new DataView(arrayBuffer);
            let offset = 0;
            //local player id
            dataView.setUint8(offset, this.id ); 
            offset += 1;
            //player data
            playerBoss.players.forEach(player => { 
                offset = player.serialize( dataView, offset );
            });
           
            socket.send(arrayBuffer);
            bytesOut += offset;

        });
        socket.on('ping', (data, flags) => {
            //@TODO: Figure out what to do with these guys ... -JCE 1-17-2017
            //console.log('ping: %s', data);
        });
        socket.on('pong', (data, flags) => {
            //console.log('pong: %s', data);
        });
    }
}

class PlayerBoss {
    constructor() {
        this.players = [];
        this.players.length = MAX_PLAYERS;
        for (let i = 0; i < this.players.length; i++) {
            this.players[i] = new Player();
            this.players[i].id = i;
        }
    }

    alloc() {
        for (let i = 0; i < this.players.length; i++) {
            let p = this.players[i];
            if (!p.active) {
                p.boss = this;
                p.active = true;
                return p;
            }
        }
        //take a bot if not enough room
        for (let i = 0; i < this.players.length; i++) {
            let p = this.players[i];
            if( p.bot )
            {
                console.log("freeing bot to make space");
                p.boss.free(p);
                return playerBoss.alloc();   
            }
        }
        return null;
    }

    getPlayer( x ) {
        x = clamp( x, 0, this.players.length );
        return this.players[ x ];
    }

    free(player) {

        player.boss = null;  
        player.bot = false;
        player.active = false;
        player.ready = false;

    }

    getActivePlayerCount() {
        let activeCount = 0;
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].active) {
                ++activeCount;
            }
        }
        return activeCount;
    }
}
let playerBoss = new PlayerBoss();

const kWebSocketPort = 8081;
let wss = new ws.Server({ port: kWebSocketPort });
wss.on('error', (error) => {
    console.log("ws error:", error);
});
wss.on('headers', (headers) => {
    console.log("ws headers:", headers);
});
wss.on('connection', (socket) => {
    console.log("ws connection:");

    // Attempt to allocate a new player for the socket
    let player = playerBoss.alloc();
    if (player) {
        player.setSocket(socket);
    } else {
        socket.close();
    }
});

//
// Update loop
//
let timestampOld = 0.0;
let timeStampDelay = 1.0;
let timeStampCount = 0;
let timeStampAvg = 0;


function update() {
    
    // Get dt
    let hrtime = process.hrtime();
    let timestamp = hrtime[0] + 1.0e-9 * hrtime[1];
    let dt = Math.min(0.1, timestamp - timestampOld);
    timestampOld = timestamp;

    //my awesome profliling acheivement...
    timeStampAvg += dt;
    timeStampCount ++;
    timeStampDelay -= dt;
    if( timeStampDelay <= 0 )
    {
        var util = require('util');
        var memoryUsage = util.inspect(process.memoryUsage());
        timeStampDelay = 5.0;
        timeStampAvg /= timeStampCount;
        bytesOut /= timeStampDelay;
        bytesIn /= timeStampDelay;
        console.log( "-------------------------");
        console.log( "Status - Average FPS:"+1.0 / timeStampAvg+" "+playerBoss.getActivePlayerCount()+" players" );
        console.log( "Memory - "+memoryUsage );
        console.log( "Network - Down:"+bytesIn/1000+" kb/s Up:"+bytesOut/1000+" kb/s" );
        timeStampAvg = 0;
        timeStampCount = 0;
        bytesOut = 0;
        bytesIn = 0;
    }

    let bots = 0;
    playerBoss.players.forEach(player => { 
        if( player.active && player.ready )
        {
            if( player.bot )
            {
                bots++; 
            }
        }
    });

    //add bots as needed
    if( bots < NUM_BOTS && playerBoss.getActivePlayerCount() < MAX_PLAYERS ){ 
        let player = playerBoss.alloc();
        player.bot = true;
        player.ready = true;
    }

}

setInterval(update, 10); //60ish fps


function compress16( dataView, offset, value, range )
{
    value = clamp( value, 0, range );
    dataView.setUint16( offset, Math.floor( value / range * 0xFFFF) ); 
    offset += 2;
    return offset;
}

// helper functions, repeated here because server.js is standalone
function clamp(v, min, max) {
    return Math.max(Math.min(v, max), min);
}

function lerp(t, a, b) {
    return a + t * (b - a);
}

function pilerp(t, a, b) {
    a = modpi( a );
    b = modpi( b );
    if( Math.abs( a - b ) > Math.PI ){
        if( a < 0 ) a += Math.PI * 2.0;
        else b += Math.PI * 2.0;
    }
   return modpi( lerp( t, a, b ) );
}

function modpi( v ) {
    v = v % ( Math.PI * 2 );
    if( v > Math.PI ) v -= Math.PI * 2;
    if( v < -Math.PI ) v += Math.PI * 2;
    return v;
}

function relativeWrap( a, b, size )
{
    if (a instanceof Vector2 && b instanceof Vector2 ) {
            if( a.x > b.x + size * 0.5 ) a.x -= size;
            if( a.x < b.x - size * 0.5 ) a.x += size;
            if( a.y > b.y + size * 0.5 ) a.y -= size;
            if( a.y < b.y - size * 0.5 ) a.y += size;
        }
    else
        throw new Error('Arguments must be of type Vector2'); 
}

function playerDataSortFunction(a,b){
    return b.pop - a.pop;   
}

function boidLifetimeSort(a,b){
    return b.lifetime - a.lifetime;
}
