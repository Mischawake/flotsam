#!/bin/bash

# First argument should be the server name we're getting the IP for
SERVER_NAME=$1

# First try to load the IP from the IP cache
IP_CACHE=./tmp/${SERVER_NAME}IpCache.txt
if [ -e $IP_CACHE ]; then
	PUBLIC_IP=$(<$IP_CACHE)
	if [[ $PUBLIC_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
		echo "$PUBLIC_IP"
		exit 0
	fi
fi

# If that doesn't work, try to grab the IP from AWS
PUBLIC_IP=`aws ec2 describe-instances --filters "Name=tag:Name,Values=${SERVER_NAME},Name=instance-state-name,Values=running" --query 'Reservations[0].Instances[0].PublicIpAddress' --output text`
if [[ !($PUBLIC_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$) ]]; then
	>&2 echo "Couldn't find IP address for '$SERVER_NAME'. Request returned '$PUBLIC_IP'. Is server running?"
	exit 1
fi
# (Cache the IP before returning)
if [ ! -d tmp ]; then mkdir tmp; fi
echo "$PUBLIC_IP" > $IP_CACHE

echo "$PUBLIC_IP"
