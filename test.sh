#!/bin/bash

SERVER_NAME=Murmuration
KEY_PATH=~/.aws/private-keys/mjh-key-pair-uswest2.pem

PUBLIC_IP=`./get-ip.sh $SERVER_NAME` || exit 1

# Launch the client page in chrome
open -a "/Applications/Google Chrome.app/" "http://${PUBLIC_IP}:8080"
