class PlayerData{

	constructor(){
		this.name = "Unnamed";
	}

}

class PlayerDataBoss{

	constructor( numPlayers ){
		this.players = [];
		for( let i = 0; i<numPlayers; i++ )
		{
			this.players[i] = new PlayerData();
		}	
	}
	getPlayer( idx ){
		return this.players[idx];
	}

	deserialize( dataView, offset ){

		let id = dataView.getUint8(offset);
        offset += 1;
        let active = dataView.getUint8(offset);
        this.players[id].active = (active === 1);
        offset += 1;
  		let nameLength =  dataView.getUint8(offset);
   		offset += 1;
   		let name = "";
   		for (let x = 0; x < nameLength; x++){
            name += String.fromCharCode( dataView.getUint8(offset) );
            offset += 1;
        }
  		this.players[id].name = name;

  		return offset;
	}

	getPlayerName(id)
	{
		return this.players[id].name;
	}

	updateBoard( board ){

		let html = "<table style='width:100%'>";
		html += "<tr span style='color:white; text-align:left'><th>Players</th></tr>";
		let count = 0;
		this.players.forEach(player => { 
			if( player.active ) count++;
        	if( player.active ){ 
        		debugHud.addLine( player.name );
        	}    
    	});	

    	//board.innerHTML = html;
    	debugHud.addLine( count+" Players" );
    	
	}
}

function playerDataSortFunction(a,b){
	return b.pop - a.pop;	
}
