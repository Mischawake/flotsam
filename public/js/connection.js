class Connection {
    constructor(url) {
        this.socket = new WebSocket(url);
        this.socket.binaryType = "arraybuffer";
        this.socket.onclose = (event) => {
            this.socket = null;
        };
        this.socket.onerror = (event) => {
            console.log("socket.onerror:", event.data);
        };
        this.socket.onmessage = (event) => {
            this.sendsSinceRecv = 0;
            this.recvBuffer.push(event.data);
        };
        this.socket.onopen = (event) => {
            this.socket.send(new ArrayBuffer(0));
        };
        //this.socket.close(in optional unsigned long code, in optional DOMString reason);

        this.recvBuffer = [];
        this.sendInterval = 1.0 / 10.0;
        this.sendTicker = this.sendInterval;
        this.sendsSinceRecv = 0;
    }

    pumpRecv() {
        if (this.recvBuffer.length === 0) {
            return null;
        } else {
            return this.recvBuffer.splice(0, 1)[0];
        }
    }

    pumpSend(dt) {
        // WebSocket state
        const CONNECTING = 0;
        const OPEN = 1;
        const CLOSING = 2;
        const CLOSED = 3;

        //@TODO: If we fail to reach the server, we should tell the player or retry, or something!
        if (!this.socket || this.socket.readyState != OPEN) {
            return false;
        }

        // If enough time has passed, and the server is still sending us messages, send something to the server
        this.sendTicker += dt;
        if (this.sendTicker > this.sendInterval && this.sendsSinceRecv < 3) {
            this.sendTicker = Math.min(this.sendTicker - this.sendInterval, this.sendInterval);
            return true;
        }
        return false;
    }

    send(data) {
        ++this.sendsSinceRecv;
        this.socket.send(data);
    }
}
