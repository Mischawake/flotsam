// adds lines of text to a given container, must be called each frame
class DebugHud
{
	constructor( container )
	{
		this.content = [];
		this.idx = 0;
		this.container = container;
	}

	addLine( text )
	{
		this.content[ this.idx ] = text;
		this.idx++;
	}

	flush()
	{
		let text = "DebugHud";
		for( let i = 0; i < this.content.length; i++ )
		{
			text += "<br>" + this.content[i];
		}
		this.container.innerHTML = text;
		this.content = [];
		this.idx = 0;
	}
}

let MAX_LINES = 32000;

class DebugLines
{
	constructor( scene )
	{
		this.scene = scene;
		this.geometryBuffer = new THREE.BufferGeometry();
		this.geometryBuffer.dynamic = true;
		let positions = new Float32Array( MAX_LINES * 6 );
        let colors = new Float32Array( MAX_LINES * 6 );
        this.geometryBuffer.addAttribute('position', new THREE.BufferAttribute(positions, 3));
        this.geometryBuffer.addAttribute('color', new THREE.BufferAttribute(colors, 3));
        let material = new THREE.LineBasicMaterial({ vertexColors: THREE.VertexColors });
        this.mesh = new THREE.LineSegments(this.geometryBuffer, material);
		scene.add(this.mesh);
		this.nextIdx = 0;
	}

	addVertex(v, color) 
	{
		let colors = this.geometryBuffer.attributes.color.array;
        let positions = this.geometryBuffer.attributes.position.array;
       
		if ( this.nextIdx >= MAX_LINES * 6 ) throw new Error("Too many points");
		
		positions[ this.nextIdx ] = v.x;
		positions[ this.nextIdx + 1 ] = v.y;
		positions[ this.nextIdx + 2 ] = v.z;

		colors[ this.nextIdx ] = color.r;
		colors[ this.nextIdx + 1 ] = color.g;
		colors[ this.nextIdx + 2 ] = color.b;

		return this.nextIdx += 3;
	}

	addLine(a, b, color)
	{
		this.addVertex( a, color );
		this.addVertex( b, color );
	}

	addLine2D(a, b, color)
	{
		this.addVertex( new THREE.Vector3(a.x, 0, -a.y ), color );
		this.addVertex( new THREE.Vector3(b.x, 0, -b.y ), color );
	}

	addArrow(a, b, radius, color )
	{
		this.addLine( a, b, color );

		let length = a.distanceTo( b );
		radius = length * radius / 2.0;
		let bToA = new THREE.Vector3().subVectors( a, b ).normalize();
		let tipCenter = new THREE.Vector3().addVectors( b, bToA.clone().multiplyScalar( radius * 2.0 ) );

		let x = bToA.clone();
		let y = new THREE.Vector3();
		let z = new THREE.Vector3();

		if( Math.abs(x.x) >= 0.57735 )
			y.set( x.y, -x.x, 0.0 );
		else 
			y.set( 0.0, x.z, -x.y );

		x.normalize();
		z.crossVectors( x, y );

		let segments = 18;
		let lastPoint = new THREE.Vector3();
		let newPoint = new THREE.Vector3();
	
		for( let i = 0; i <= segments; i++ )
		{	
			let theta = Math.PI * 2 / segments * i;
			newPoint.copy( tipCenter );
			newPoint.add( y.clone().multiplyScalar( Math.sin( theta ) * radius ) );
			newPoint.add( z.clone().multiplyScalar( Math.cos( theta ) * radius ) );
			if( i > 0 ) this.addLine( lastPoint, newPoint, color );
			
			if( i % 3 <= 0) this.addLine( newPoint, b, color );
			lastPoint.copy( newPoint );
		}
	}

	addSphere( center, radius, color )
	{
		this.addCircle( center, radius, new THREE.Vector3(1,0,0), color );
		this.addCircle( center, radius, new THREE.Vector3(0,1,0), color );
		this.addCircle( center, radius, new THREE.Vector3(0,0,1), color );
	}

	addCircle( center, radius, normal, color, segments = 18 )
	{
		let a = normal.normalize();
		let b = new THREE.Vector3();
		let c = new THREE.Vector3();

		if( Math.abs(a.x) >= 0.57735 )
			b.set( a.y, -a.x, 0.0 );
		else 
			b.set( 0.0, a.z, -a.y );

		b.normalize();
		c.crossVectors( a, b );

		let lastPoint = new THREE.Vector3();
		let newPoint = new THREE.Vector3();
	
		for( let i = 0; i <= segments; i++ )
		{	
			let theta = Math.PI * 2 / segments * i;
			newPoint.copy( center );
			newPoint.add( b.clone().multiplyScalar( Math.sin( theta ) * radius ) );
			newPoint.add( c.clone().multiplyScalar( Math.cos( theta ) * radius ) );
			if( i > 0 ) this.addLine( lastPoint, newPoint, color );
			lastPoint.copy( newPoint );
		}
	}

	flush()
	{

		this.geometryBuffer.attributes.position.needsUpdate = true;
        this.geometryBuffer.attributes.color.needsUpdate = true;
		this.geometryBuffer.attributes.position.count = this.nextIdx / 3 ;
		this.geometryBuffer.attributes.color.count = this.nextIdx / 3;
		this.mesh.frustumCulled = false;
		this.nextIdx = 0;
	}
}