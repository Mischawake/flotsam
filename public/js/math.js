
function compress16( dataView, offset, value, range )
{
    value = clamp( value, 0, range );
    dataView.setUint16( offset, Math.floor( value / range * 0xFFFF) ); 
}

function decompress16( dataView, offset, range )
{
    let value = dataView.getUint16( offset );
    value /= 0xFFFF;
    value *= range;
    return value;
}

function clamp(v, min, max) {
    return Math.max(Math.min(v, max), min);
}

function lerp(t, a, b) {
    return a + t * (b - a);
}

function getPixelToWorldUnits( camera )
{
    let frustum = camera.right * 2;
    let pixels = window.innerWidth;

    return frustum / pixels;
}

function pilerp(t, a, b) {
    a = modpi( a );
    b = modpi( b );
    if( Math.abs( a - b ) > Math.PI ){
        if( a < 0 ) a += Math.PI * 2.0;
        else b += Math.PI * 2.0;
    }
   return modpi( lerp( t, a, b ) );
}

function modpi( v ) {
    v = v % ( Math.PI * 2 );
    if( v > Math.PI ) v -= Math.PI * 2;
    if( v < -Math.PI ) v += Math.PI * 2;
    return v;
}

function delerp(t, a, b) {
    if( a == b ) return 0;
    let max = Math.max( a, b );
    let min = Math.min( a, b );
    return (t - min) / (max - min);
}

function lerpVector3(t, a, b) {
    let r = new THREE.Vector3();
    r.x = lerp(t, a.x, b.x);
    r.y = lerp(t, a.y, b.y);
    r.z = lerp(t, a.z, b.z);
    return r;
}

function wrapVector3(a, size)
{
    let offset = new THREE.Vector3();
    //x
    if( a.x > size * 0.5 ){
        offset.x -= size;
    } 
    else if( a.x < size * -0.5 ){ 
       offset.x += size;
    }
    //y
    if( a.y > size * 0.5 ){
       offset.y -= size;
    } 
    else if( a.y < size * -0.5 ){ 
        offset.y += size;
    }
    //z
    if( a.z > size * 0.5 ){
        offset.z -= size;
    } 
    else if( a.z < size * -0.5 ){ 
        offset.z += size;
    }
    //
    a.add( offset );
    return offset;
}

function wrapVector2(a, size)
{
    let offset = new THREE.Vector2();
    //x
    if( a.x > size * 0.5 ){
        offset.x -= size;
    } 
    else if( a.x < size * -0.5 ){ 
       offset.x += size;
    }
    //y
    if( a.y > size * 0.5 ){
       offset.y -= size;
    } 
    else if( a.y < size * -0.5 ){ 
        offset.y += size;
    }
    a.add( offset );
    return offset;
}


function randomUnitVector3(){

    return new THREE.Vector3( Math.random() * 2 - 1, Math.random() * 2 - 1, Math.random() * 2 - 1 ).normalize();
}

function randomUnitVector2(){

    return new THREE.Vector2( Math.random() * 2 - 1, Math.random() * 2 - 1 ).normalize();
}

function randomAbsRange( range )
{
    return (Math.random() * 2 - 1) * range / 2;
}

function worldToScreenspace(worldPos, camera) {

    let pos = worldPos.clone();
    let viewProj = new THREE.Matrix4();
    viewProj.multiplyMatrices(camera.projectionMatrix, camera.matrixWorldInverse);
    pos.applyProjection(viewProj);

    let vector = new THREE.Vector2(
        Math.round((1 + pos.x) * window.innerWidth / 2),
        Math.round((1 - pos.y) * window.innerHeight / 2)
    );

    return vector;
}

function screenToWorldSpaceXZPlane( screenPos, camera )
{
    let posNorm = new THREE.Vector2( ( screenPos.x  / window.innerWidth ) * 2 - 1, 
        ( -screenPos.y / window.innerHeight ) * 2 + 1 );
    let worldPos = new THREE.Vector3( camera.position.x + posNorm.x * camera.right, 0, camera.position.z - posNorm.y * camera.top );
    return worldPos;
}


// Taken from http://libnoise.sourceforge.net
// Modified to a 0 - 1 range
function intNoise(n) {
    n = (n >> 13) ^ n;
    let nn = (n * (n * n * 60493 + 19990303) + 1376312589) & 0x7fffffff;
    return (nn / 1073741824.0) * 0.5;
}
