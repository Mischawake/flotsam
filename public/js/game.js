let canvas, ctx, scene, renderer, stats;
let camera, cameraLook, cameraLookTarget, cameraFrustum;
let connection;
let input;
let textManager;
let raycaster;
let debugLines;
let debugHud;

let playerDataBoss;

let mousePos;

//
let localPlayerID;

//color short cuts
let color_red = new THREE.Color( 0xff0000 );
let color_green = new THREE.Color( 0x00ff00 );
let color_blue = new THREE.Color( 0x0000ff );
let color_yellow = new THREE.Color( 0xffff00 );
let color_black = new THREE.Color( 0x000000 );
let color_cold = new THREE.Color( 0x046eff );
let color_heat = new THREE.Color( 0xff0427 );
let color_white = new THREE.Color( 0xffffff );

const MAX_PLAYERS = 2; // must match with server

let playerReady;

function startButton() {
    document.getElementById("splashHolder").style.visibility = "hidden";
    playerReady = true;
}

function init() {
    canvas = document.getElementById('canvas');
    //canvas.style.cursor = "none";
    ctx = canvas.getContext('2d');

    scene = new THREE.Scene();
    
    playerStart = false;
    scene.fog = new THREE.Fog(0x000000, 2000, 3500);

    let amb1 = new THREE.AmbientLight(0x444444);
    scene.add(amb1);

    let dir1 = new THREE.DirectionalLight(0xffffff, 1.0);
    dir1.position.set(0, 1, 1);
    dir1.castShadow = false;
    scene.add(dir1);

    let aspect = window.innerWidth / window.innerHeight;
    cameraFrustum = 150;
    camera = new THREE.OrthographicCamera( cameraFrustum * aspect / - 2, cameraFrustum * aspect / 2, cameraFrustum / 2, cameraFrustum / - 2, 1, 1000 );
    camera.aspect = aspect;
    camera.rotation.x = -Math.PI / 2.0; //looking down
    camera.rotation.y = 0;
    camera.rotation.z = 0;
    scene.add(camera);
    
    cameraLook = new THREE.Vector3(0, 0, 0);
    cameraLookTarget = new THREE.Vector3(0, 0, 0);
    debugLines = new DebugLines( scene );
    debugHud = new DebugHud( document.getElementById("debugHud") );
    input = new Input(); //
    textManager = new TextManager();
    raycaster = new THREE.Raycaster();
    
    playerDataBoss = new PlayerDataBoss( MAX_PLAYERS );
    mousePos = new THREE.Vector2();
    localPlayerID = 0;

    renderer = new THREE.WebGLRenderer({
        antialias: true
    });

    renderer.setClearColor(scene.fog.color);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = false;

    let container = document.getElementById('webgl-container');
    container.appendChild(renderer.domElement);

    stats = new Stats();
    stats.showPanel(0);
    container.appendChild(stats.dom);

    let webSocketUrl = "ws://"+window.location.hostname+":8081";
    connection = new Connection(webSocketUrl);

    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    window.addEventListener('resize', onWindowResize, false);
    onWindowResize();
}

function onDocumentMouseMove( event ) {
    mousePos.x = event.clientX; 
    mousePos.y = event.clientY;
}

function onWindowResize() {
    
    let aspect = window.innerWidth / window.innerHeight;
    camera.aspect = aspect;

    let targetArea = cameraFrustum * cameraFrustum;

    camera.left   = - ( cameraFrustum ) / 2;
    camera.right  =   ( cameraFrustum ) / 2;
    camera.top    =   ( cameraFrustum ) / aspect / 2;
    camera.bottom = - ( cameraFrustum  ) / aspect / 2; 

    // keep area consistent:
    let w = camera.right * 2;
    let h = camera.top * 2;
    let area = w * h;
    let zoom = Math.sqrt( targetArea / area );

    zoom = clamp( zoom, 0, 2);

    camera.left *= zoom;
    camera.right *= zoom;
    camera.top *= zoom;
    camera.bottom *= zoom;

    renderer.setSize( window.innerWidth, innerHeight );
    canvas.width = window.innerWidth;
    canvas.height = innerHeight;
    camera.updateProjectionMatrix();

}

let prevTime = 0;

function update(time) {

    if( connection.socket === null )
    {
        document.getElementById("signin").style.display = "none";
        document.getElementById("error").style.display = "block";
        return;
    }

    let dt = (time - prevTime) / 1000;
    prevTime = time;
    dt = clamp( dt, 0.001, 0.1 ); //prevents weirdness when away from tab
    
    let localPlayer = playerDataBoss.getPlayer(localPlayerID); 
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    // CAMERA
    let snap = dt * 8;
    cameraLook = lerpVector3(snap, cameraLook, cameraLookTarget );
    let cameraPos = new THREE.Vector3().addVectors(cameraLook, new THREE.Vector3(0,50,0) );
    camera.position.set(cameraPos.x, cameraPos.y, cameraPos.z);
    camera.updateProjectionMatrix();


    debugLines.addLine( new THREE.Vector3(50,0,50), new THREE.Vector3(-50,0,-50), color_red );
    debugLines.addLine( new THREE.Vector3(50,0,-50), new THREE.Vector3(-50,0,50), color_red );

    //NETWORKING
    let data = null;
    while ((data = connection.pumpRecv())) {
        let dataView = new DataView(data);
        let offset = 0;

        localPlayerID = dataView.getUint8(offset);
        offset += 1;
     
        for( let i = 0; i < MAX_PLAYERS; i ++ ){
            offset = playerDataBoss.deserialize( dataView, offset );
        }
    }
  
    if (connection.pumpSend(dt)) {
        // Serialize our data
        
        //let interact = input.keyDown(Key.Mouse) || input.keyDown(Key.Space);
        let arrayBuffer = new ArrayBuffer(1+16);
        let dataView = new DataView(arrayBuffer);
        let offset = 0;
    
        dataView.setUint8( offset, playerReady ? 1 : 0 );
        offset += 1;
        //
        let playerName = localPlayer.name;
        //player name :( really do not need to be sending each time...
        let nameLength = playerName.length;
        if( nameLength > 15 ) nameLength = 15; //max 
        dataView.setUint8(offset, nameLength );
        offset += 1;
        for (let x = 0; x < nameLength; x++)
        {
            dataView.setUint8(offset, localPlayer.name.charCodeAt(x) );
            offset += 1;
        }
        connection.send(arrayBuffer); 
    }

    // UI
    localPlayer.name = document.getElementById("nickname").value; //set from input field, then propogates to server

    //UPDATE LEADER BOARD
    playerDataBoss.updateBoard( document.getElementById("leaderBoard") );
    
    renderer.render( scene, camera );
    stats.update();
    debugLines.flush();
    debugHud.flush();
    input.flush();
    requestAnimationFrame(update);
  
}

window.onload = () => {
    init();
    update(0);
};
