#!/bin/bash

SSN=serf
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

tmux has-session -t $SSN 2>/dev/null
if [ "$?" -eq 1 ]; then
    echo "Creating new session $SSN"

    tmux new-session -s $SSN -c $DIR -d;
    tmux split-window -t $SSN -c $DIR -h
    tmux send-keys -t $SSN:1.0 'grunt watch' C-m;
    tmux send-keys -t $SSN:1.1 'npm start' C-m;
    tmux new-window -t $SSN -c $DIR;
    tmux send-keys -t $SSN 'emacs -nw' C-m;
    tmux select-window -t $SSN:2
fi

tmux switch-client -t $SSN
