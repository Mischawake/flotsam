#!/bin/bash

SERVER_NAME=Murmuration

# See if the server is already running by trying to grab its public IP
echo "Searching for existing instance named $SERVER_NAME ..."
PUBLIC_IP=`aws ec2 describe-instances --filters "Name=tag:Name,Values=${SERVER_NAME}" --query 'Reservations[0].Instances[0].PublicIpAddress' --output text`
# We don't need to do anything, if it is
if [[ $PUBLIC_IP =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
	echo "Instance already running at $PUBLIC_IP."
	exit 0;
else
	echo "Instance not found."
fi

echo "Launching new instance ..."
INSTANCE_ID=`aws ec2 run-instances --image-id ami-29ebb519 --security-group-ids sg-3cb1d644 --count 1 --user-data "file://startup.sh" --instance-type m4.large --key-name mjh-key-pair-uswest2 --query 'Instances[0].InstanceId' --output text`
aws ec2 create-tags --resource ${INSTANCE_ID} --tags Key=Name,Value=$SERVER_NAME
PUBLIC_IP=`aws ec2 describe-instances --instance-ids ${INSTANCE_ID} --query 'Reservations[0].Instances[0].PublicIpAddress' --output text`

IP_CACHE=./tmp/${SERVER_NAME}IpCache.txt
echo "Caching instance IP address ($PUBLIC_IP) in $IP_CACHE."
if [ ! -d tmp ]; then mkdir tmp; fi
echo "$PUBLIC_IP" > $IP_CACHE

echo "Instance $INSTANCE_ID at $PUBLIC_IP booting ..."
aws ec2 wait instance-running --instance-ids $INSTANCE_ID

echo "Instance $INSTANCE_ID at $PUBLIC_IP initializing ..."
aws ec2 wait instance-status-ok --instance-ids $INSTANCE_ID

echo "Complete!"
