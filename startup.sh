#!/bin/bash

# Run until eof as the user "ubuntu"
exec sudo -i -u ubuntu /bin/bash - << eof

# Install htop
sudo apt-get install htop

# Install node.js (telling apt-get about the correct repo, first)
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

eof
