AWS NOTES:
1) launch-instance.sh
2) ssh.sh
3) copy-files.sh
4) in ubuntu, run npm install
5) in ubuntu, run npm start
6) test.sh - will launch browser window
7) If changes are made, re execute copy-files.sh 

GENERAL:
Explosive force when someone gets killed? Push the boids around? for one frame?
Shouldn't spawn from a boid that was just freed, maybe order the boids by lifetime, or only spawn at one that's more than x seconds old?


BUGS:
-If server restarts game crashes, would be nice to auto restart.
-Reserve the max player number of boids, so that players can always be added...

GAMEPLAY:

-Food restores energy, energy gives you a shield?
-Boid combat doesn't feel balenced, being big needs more drawbacks
-A random pickup that would give a little guy a chance to take out a big dude - mario star? temp invincibility?
-Mess with momentum for boids, could be heavier feeling?
-??? Boid health so that they can take more than 1 hit?
-More trail / drawing feel for guiding boids


TECH:
-Finish implementing trails for flyers using a geo buffer
-Compression strategy 

#########################################################

FINISHED:

-Teams build 
-Would be good to keep boids in family buckets for easy accsess?
-Convert flyer representation 
-Convert forrest
-Convert debuglines
-Spacial Hash for boids
-Mixing canvas with perspective camera looks funky... Try and orthographic camera
-ArrayBuffers probably not being released properly
-Switch to using zoom instead of moving viewport
-Setup git client for better tracking
-Intro screen with name entry
-Leader board 
-"DebugHud"
-Use position between heart and avg of rest of flock (or maybe just furthest away) to control zoom. If camera follows heart, zoom will determine attack range.
-Try "brake" concept for inputting 
-Add player count readout
-Add orbit boid behvaior
-Click/Space should do something (hooked up)
-HSV Color for players
-Finish implementing boids, add wander behavior, clean up usage syntax
-Keyboard control options for 2 players
-Split Screen render
-Boids flocking is broken, probably because of removing max turn rate
-Free wandering boids that you can "eat"
-Combat mechanic
-Collision center for boids looks off 
-Aspect ratio cheat, making screen wider should crop instead of reframe
-Setup AWS
-Player pos off in raycast
-Boid collisions have some bounce physics
-Finish implementing flocking behaviors
-Board edge flocking behavior broken!!!!!!!!!
-Evenly 'spaced' colors shuffled for player colors
-Special reward for taking out another player
	Get to keep their 'head'
	Adds a third type of boid to your flock, a head that can also eat free boids
	Reverts back to a normal boid when killed (so that killing a player only ever gives you one head? )
	Heads should auto eat free boids
-On Click:
	Change something in flocking dynamic / formation 
	Ping/Shout
