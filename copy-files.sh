#!/bin/bash

SERVER_NAME=Murmuration
KEY_PATH=~/.aws/private-keys/mjh-key-pair-uswest2.pem

PUBLIC_IP=`./get-ip.sh $SERVER_NAME` || exit 1

# Copy all the files necessary for running the project
rsync -air -e "ssh -i ${KEY_PATH}" Gruntfile.js package.json public src ubuntu@${PUBLIC_IP}:~
